package com.martynas.palindromedates;

import com.martynas.palindromedates.domain.Palindrome;
import javax.management.InvalidAttributeValueException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PalindromeDatesApplication {

    public static void main(String[] args) throws InvalidAttributeValueException {
        SpringApplication.run(PalindromeDatesApplication.class, args);

        int y1 = 2010;
        int y2 = 2015;
        Palindrome.printBonusDatesBetween(y1, y2);
    }
}
