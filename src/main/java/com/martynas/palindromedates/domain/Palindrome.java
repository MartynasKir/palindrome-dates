package com.martynas.palindromedates.domain;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import javax.management.InvalidAttributeValueException;

public class Palindrome {

    public static void printBonusDatesBetween(int fromYear, int toYear) throws InvalidAttributeValueException {

        if (fromYear > toYear) {
            throw new InvalidAttributeValueException(fromYear + " is value more than " + toYear);
        }
        getPalindromeDatesList(fromYear, toYear).forEach(System.out::println);
    }


    static List<LocalDate> getPalindromeDatesList(int start, int end) {

        List<LocalDate> palindromeDates = new ArrayList<>();

        for (int i = start; i < end; i++) {

            LocalDate startDate = LocalDate.of(i, 1, 1);
            while (startDate.isBefore(LocalDate.of(i, 12, 31))) {
                String startDateString = startDate.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
                if (isPalindrome(startDateString)) {
                    palindromeDates.add(startDate);
                    break;
                }
                startDate = startDate.plusDays(1);
            }
        }
        return palindromeDates;
    }


    private static boolean isPalindrome(String str) {
        int i = 0;
        int j = str.length() - 1;

        while (i < j) {

            if (str.charAt(i) != str.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}
