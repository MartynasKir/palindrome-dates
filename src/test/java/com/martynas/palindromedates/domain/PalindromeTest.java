package com.martynas.palindromedates.domain;

import java.time.LocalDate;
import java.util.List;
import javax.management.InvalidAttributeValueException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class PalindromeTest {

    @Test
    void givenValidParameters_returnPalindromeDates() {

        //given
        int y1 = 2010;
        int y2 = 2015;

        List<LocalDate> expectedPalinromeDates = List.of(
                LocalDate.of(2010, 1, 2),
                LocalDate.of(2011, 11, 2));

        //when
        List<LocalDate> actualPalindromeDates = Palindrome.getPalindromeDatesList(y1, y2);

        //then
        Assertions.assertEquals(expectedPalinromeDates.get(0), actualPalindromeDates.get(0));
    }

    @Test
    void givenInvalidParameters_returnException() {

        //given
        int y1 = 2015;
        int y2 = 2010;

        //then
        Assertions.assertThrows(InvalidAttributeValueException.class, () ->
                Palindrome.printBonusDatesBetween(y1, y2));
    }
}
